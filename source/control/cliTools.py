import sys
import time
import re
import subprocess
#------------------------------
#      colorize
#------------------------------
def colorize(string, color=None, reverse = False, underline = False):
    """
    returns a text formatted so it will be displayed in a bash console
    with the wanted color
    #red{asdfasfd}
    
    """

    string = str(string)

    colors = {
        "white": "39m",
        "brightwhite": "1m",
        "black": "30m",
        "grey": "1;30m",
        "red": "31m",
        "lightred": "1;31m",
        "green": "32m",
        "lightgreen": "1;32m",
        "orange": "33m",
        "yellow": "1;33m",
        "blue": "34m",
        "lightblue": "1;34m",
        "magenta": "35m",
        "pink": "1;35m",
        "lightmagenta": "1;35m",
        "cyan": "36m",
        "lightcyan": "1;36m",
        "lightgrey": "37m",
        "darkwhite": "1;37m", 
        "normal": "1;0m"
    }
    
    # 1) replace back to normal to back to this color (because this
    # color is now what "normal" is
    ESC = "\033["
    END = "\033[0m"
    
    string = string.replace(END, ESC + colors[color])
    
    # 2) apply color to the beginning of the string
    
    if color is not None:
        
        colorCode = colors[color]
        
        if reverse: 
            colorCode = colorCode.replace("0;3", "4")
            colorCode = colorCode.replace("1;3" , "4")

        if underline: 
            colorCode = colorCode.replace("0;3", "4;3")
            colorCode = colorCode.replace("1;3" , "4;3")
    
        result =  ESC + colorCode + string + END
        return result
     
    else: # color is introduced partially using ${}
        for c in colors:
            pattern = "$" + c + "{([^}]*?)}"
            while 1:
                m = re.search(pattern, string)
                if m is None: break
                start = ESC + colors[c]                
                string = string[:m.pos(0)] + start + m.group(1) + END + string[m.pos(0) + len(m.group(0)):]
                
        return string



############ INPUT FAMILY ################
  
dInputQueue = []
#------------------
# TextConsole
#------------------
class TextConsole:

    """
    implements a identable console
    """

    def __init__(self):
        self.depth = 0
        self.writeLn()

    def write(self, txt, depth=0):
        sys.stdout.write(txt)

    def input(self, prompt="", default=None, helpStr=None):

        if help is not None:
            prompt = prompt + "(" + helpStr + ")"

        s = xInput(prompt + " ")
        print(" " * self.depth, end=' ')
        if s == "" and default is not None:
            s = default

        return s

    def writeLn(self, str_="", indent=0):
        print(str_)
        self.depth = max(0, self.depth + indent)
        sys.stdout.write(" " * self.depth)

    def writeLine(self, str_="", indent=0):
        self.writeLn(str_, indent)

    def getProgressMonitor(self):
        return TextProgressMonitor(50)

    def indent(self):
        self.writeLine("", 4)

    def dedent(self):
        self.writeLine("", -4)




#--------------------
def getTermSize():
    """
    returns the size of the terminal    
    """
    try:
        rows, columns = subprocess.check_output(['stty', 'size']).split()
    except:
        rows, columns = 20,80
    return int(rows), int(columns)
    
#------------------
# xInput
#------------------
def xInput(prompt, inject = None):
    """
    keyboard input allowing for debug
    @param inject answer, used to debug
    """
    if len(dInputQueue) != 0:
        return dInputQueue.pop(0)
    
    elif inject != None:
        return inject
    
    else:
        return input(prompt)


 

#--------------------
#  readString
#--------------------
def readString(prompt, default=None, condition=lambda x: True):
    """
    reads an int from the console
    """
    while True:
        s = prompt

        if default:
            s += "[" + default + "]"

        s = xInput(s)

        if (s == "") and default is not None:
            s = default
            print(s)

        if condition(s):
            break  # break only if pass condition

    return s

 



#---------------------
#   readInt
#---------------------
def readInt(prompt, condition = lambda x: True, default = None):
    """
    reads an int from the console
    """
    while 1:
        s = xInput(prompt)
        try:
            if s == "": 
                 i = ""
                 raise ValueError()  # eval fails on empty strings
            i = int(eval(s));
            if condition(i): break # break only if pass condition
        except ValueError:
            if i == "" and default != None:
                return int(default)
        except SyntaxError:
            if i == "" and default != None:
                return int(default)
            else:
                print("syntax error")
    return i


#------------------------
#   readFloat
#-----------------------
def readFloat(prompt, default = None, condition = lambda x: True):
    """
    reads an int from the console
    """
    if default != None: prompt += "[%s]" % default
    while 1:
        s = xInput(prompt)
        try:
            if s == "" and default != None:
                s = default
            i = float(s);
            if condition(i): break # break only if pass condition
        except ValueError:
            pass
    return i


#-----------------------------
#  readYn
#-----------------------------
def readYn(prompt):
    """
    """
    while 1:
        x = input(prompt).strip().lower()
        if x == "yes" or x == "y":
            return True
        elif x == "no" or x == "n":
            return False
        
#-------------------------------
def confirm(prompt):
    return readYn(prompt)

#------------------------------------
def parseTime(s):
        """        
        """
        def part2(text, separator):
            a, s, b = text.partition(separator)
            if s == "":
                return b, s, a
            else:
                return a, s, b
        
        days, separator, rest = part2(s, "d")        
        days = int(days) if separator != "" else 0
        hours, separator, rest = part2(rest, "h")
        hours = int(hours) if separator != "" else 0
        mins, separator, rest = part2(rest, "m")
        mins = int(mins) if separator != "" else 0
        secs, separator, rest = part2(rest, "s")
        secs = int(secs) if separator != "" else 0
        
        if rest.strip() != "":
            raise ValueError("wrong format for time (must be in dhms format")
        
        x = secs + mins*60 + hours * 3600 + days * 86400
        return x

#-----------------------------
def readTime(prompt, default = None, inject = None, filter_ = lambda x: True):
    """
    reads time in the form 33d23h12m21s
    """
        
    while 1:
        s = readString(prompt, default, inject = inject)
        try:
            x = parseTime(s)
            if not list(filter_(x)):
                print("error")
                continue 
            return x
        
        except ValueError:
            print("wrong format: expected .d.m.h")
    
#---------------------------------------    
def readDate(prompt, default = None, inject = None):
    while 1:
        s = readString(prompt, default, inject = inject)
        try:
            return time.mktime(time.strptime(s, "%d/%m/%Y"))            
        except ValueError:
            print ("wrong format. Must be dd/mm/yyyy")
        
#---------------------
#  readFormat
#---------------------
def readFormat(prompt, sFormat, default = None):
    """
    reads an int from the console
    """
    while 1:
        s = prompt

        if default:
            s += "[" + default + "]"

        s = xInput(s)

        if (s == "") and default != None:
            s = default
            print( s )

        if re.match(sFormat, s):
            break
        else:
            print("wrong format")

    return s
  
#------------------------------------
# inputChoice
#------------------------------------
def inputChoice(prompt, choices):
    """
    """
    while 1:
        s = input(prompt).lower().strip()
        if s in choices:
            return choices[s]

      
#------------------------------------
# inputInt
#------------------------------------
def inputInt(prompt, test = lambda x:True, default = None, shortcuts = None):
    """
    """
    if shortcuts == None: shortcuts = {}
    while 1:
        s = input(prompt).lower().strip()
        s = shortcuts.get(s, s)
        if s == "" and default != None:
            return int(default)
  
        try:
            if test(int(s)): return int(s)
            continue
  
        except:
            continue
        
#----------------------
#
#----------------------

def menu(opciones):
    """
    """
    l = len(opciones)
    for o in range(1, l + 1):
        print(str(o) + ") " +\
            opciones[o - 1])

    while True:
        try:
            i = int(xInput("?"))
            if i > 0 and i <= l:
                break
        except ValueError:
            pass

    return i - 1

