
[1;33m+-----------------------------+[0m
[1;33m| application in dummy mode:  |[0m
[1;33m|  no connection established  |[0m
[1;33m+-----------------------------+[0m
[1m
----------------------------------------------
F) move forward        %) (movement test 2)
B) move backward       &) (movement test 1)
0) disable motor       W) (swing test)
1) enable motor        C) comms check
S) perform sweep       R) raw command
X) exit
----------------------------------------------[0m
[31m  [MOTOR DISABLED][0m
>[1m
----------------------------------------------
F) move forward        %) (movement test 2)
B) move backward       &) (movement test 1)
0) disable motor       W) (swing test)
1) enable motor        C) comms check
S) perform sweep       R) raw command
X) exit
----------------------------------------------[0m
[31m  [MOTOR DISABLED][0m
>[1m
----------------------------------------------
F) move forward        %) (movement test 2)
B) move backward       &) (movement test 1)
0) disable motor       W) (swing test)
1) enable motor        C) comms check
S) perform sweep       R) raw command
X) exit
----------------------------------------------[0m
[31m  [MOTOR DISABLED][0m
>[1;31m***||| MOTOR IS DISABLED. ENABLE IT TO PERFORM THE SWEEP |||***[0m
[1m
----------------------------------------------
F) move forward        %) (movement test 2)
B) move backward       &) (movement test 1)
0) disable motor       W) (swing test)
1) enable motor        C) comms check
S) perform sweep       R) raw command
X) exit
----------------------------------------------[0m
[31m  [MOTOR DISABLED][0m
>[35m[application in dummy mode: nothing is being sent to the motor][0m
[1m
----------------------------------------------
F) move forward        %) (movement test 2)
B) move backward       &) (movement test 1)
0) disable motor       W) (swing test)
1) enable motor        C) comms check
S) perform sweep       R) raw command
X) exit
----------------------------------------------[0m
[32m  [MOTOR ENABLED][0m
>initial angle? increment in degrees? that is 100.00 full steps, that amounts to 9.00 degrees
number of positions? seconds per position? 


[35m[application in dummy mode: nothing is being sent to the motor][0m
--------------
pos 0) start time: 08:31:04: pos 0: steps: 0, degs:0.00 
current file: IFJ_2022_Dec_run045_221211_065501_part015.zip, size=335532032
4 secs remaining   3 secs remaining   2 secs remaining   1 secs remaining   waited 4 seconds: endtime: 08:31:08  
enabling motor
[35m[application in dummy mode: nothing is being sent to the motor][0m
moving motor....[35m[application in dummy mode: nothing is being sent to the motor][0m
motor moved 100 steps 9.00 deg
disabling motor...[35m[application in dummy mode: nothing is being sent to the motor][0m
--------------
pos 1) start time: 08:31:17: pos 1: steps: 100, degs:9.00 
current file: IFJ_2022_Dec_run045_221211_065501_part015.zip, size=354013184
4 secs remaining   3 secs remaining   2 secs remaining   1 secs remaining   waited 4 seconds: endtime: 08:31:21  
enabling motor
[35m[application in dummy mode: nothing is being sent to the motor][0m
moving motor....[35m[application in dummy mode: nothing is being sent to the motor][0m
motor moved 100 steps 9.00 deg
disabling motor...[35m[application in dummy mode: nothing is being sent to the motor][0m
--------------
pos 2) start time: 08:31:30: pos 2: steps: 200, degs:18.00 
current file: IFJ_2022_Dec_run045_221211_065501_part015.zip, size=372494336
4 secs remaining   3 secs remaining   2 secs remaining   1 secs remaining   waited 4 seconds: endtime: 08:31:34  
enabling motor
[35m[application in dummy mode: nothing is being sent to the motor][0m
moving motor....[35m[application in dummy mode: nothing is being sent to the motor][0m
motor moved 100 steps 9.00 deg
disabling motor...[35m[application in dummy mode: nothing is being sent to the motor][0m
-------
Sweep finished. Final position after 300 steps: 27.0 degrees
================
[1m
----------------------------------------------
F) move forward        %) (movement test 2)
B) move backward       &) (movement test 1)
0) disable motor       W) (swing test)
1) enable motor        C) comms check
S) perform sweep       R) raw command
X) exit
----------------------------------------------[0m
[31m  [MOTOR DISABLED][0m
>Exit script: are you sure (y/n)