#!/usr/bin/env python3

import serial
import time
import sys
import os
from cliTools import *


def newestFile(directory:str):
    """
    @param directory the directory where to look 
    @return the name and the os stats for the newest modified file in a directory
    """
    paths = [directory + "/" + f for f in os.listdir(directory)]
    return max([(f, os.stat(f)) for f in paths], key = lambda x: x[1].st_mtime)
    


MENU = """
----------------------------------------------
F) move forward        %) (movement test 2)
B) move backward       &) (movement test 1)
0) disable motor       W) (swing test)
1) enable motor        C) comms check
S) perform sweep       R) raw command
X) exit
----------------------------------------------"""

######################
DUMMY = True   # setting this flag to True stops sending messages to the serial port.
######################

BAUDRATE = 9600
secs = 1      # unit const for seconds

# --- magic number for commands ---

COMMAND_FORWARD = 0
COMMAND_BACKWARD = 1
COMMAND_HOME = 2
COMMAND_MOVE = 3
COMMAND_POS = 4
COMMAND_CHECK = 5
COMMAND_RAW = 6
COMMAND_ENABLE = 7
COMMAND_DISABLE = 8
COMMAND_ANGLE = 9

STEP_WAIT = 0.045  # this is the time that 1 step takes. used to wait for answer
DEGS_PER_STEP = 0.09  # degrees turned per motor step


class MotorControl:
    """
    main application for motor control
    """
         
    def __init__(self, dev:str):
        """
        constructor
        @param dev name of the serial port (/etc/dev/usbTTY0, for example)
        """
        print()
        
        if not DUMMY: 
            print(f"connecting to {dev}@{BAUDRATE}...")
            self.ser = serial.Serial(dev, BAUDRATE) #timeout=10);
            print(f"connected to {dev}")
        else:
            print(colorize("+-----------------------------+", "yellow"))
            print(colorize("| application in dummy mode:  |", "yellow"))
            print(colorize("|  no connection established  |", "yellow"))
            print(colorize("+-----------------------------+", "yellow"))

        self.availableCommands = {
            "F":  self.fn_moveForward,
            "C":  self.fn_commCheck,
            "B":  self.fn_moveBackward,
            "S":  self.fn_sweep,
            "R":  self.fn_rawCommand,
            "A":  self.fn_angle,
            "0":  self.fn_disable,
            "1":  self.fn_enable,
            "&":  self.fn_hardTest,
            "W":  self.fn_swing, 
        }           
        
        self.motorEnabled = False;
                
    def interactive(self):
        """
        opens the menu and waits for user selections
        """
        self.reset_buffer()
        
        while 1: 
            self.emptyInputBuffer()
                    
            print(colorize(MENU, "brightwhite"))
            if self.motorEnabled:
                print(colorize("  [MOTOR ENABLED]", "green"))
            else:
                print(colorize("  [MOTOR DISABLED]", "red"))
                
            try:
                x = input(">").strip().upper()                    
                if x in self.availableCommands:                
                    self.availableCommands[x]()                

                elif x == "X":
                    a = input("Exit script: are you sure (y/n)")
                    if a == "y":
                        break
                    else:
                        pass
        
            except KeyboardInterrupt as e:
                print("CTRL-C. use x to exit")
                continue

            time.sleep(1.5)
            
            if not DUMMY:
                time.sleep(0.1)
                while self.ser.inWaiting() > 0: 
                    print("read:", self.ser.read(self.ser.inWaiting()).decode( "utf8"))


    #------------------
    #  COMMANDS
    #------------------
    
    def fn_sweep(self):
        """
        makes a full sweep
        """
         
        if not self.motorEnabled:
            print(colorize("***||| MOTOR IS DISABLED. ENABLE IT TO PERFORM THE SWEEP |||***", "lightred"))
            return
        
        initAngle = readFloat("initial angle? ")
        deltaAngle2 = readInt("increment in degrees? ")
        steps = int(deltaAngle2 / DEGS_PER_STEP)
        deltaAngle = steps * DEGS_PER_STEP
        
        # check that results in an integer number of steps
        if abs(deltaAngle2 - deltaAngle ) > 0.01:
            print(colorize("WARNING: that results in %2.2f full steps, that amounts to %2.2f degrees" % (steps, deltaAngle), "yellow")),
        else:
            print("that is %2.2f full steps, that amounts to %2.2f degrees" % (steps, deltaAngle)),
            
        times = readInt("number of positions? ")
        t = readInt("seconds per position? ")
        totalSteps = 0
        print("\n\n")
        self.write(COMMAND_DISABLE, 0 ,0)
        time.sleep(1)
        self.emptyInputBuffer()
        currentAngle = initAngle

        for i in range(times):
            
            # 1) wait
            print("--------------")
            print(f"pos {i}) start time: {time.strftime('%H:%M:%S')}: pos {i}: steps: {totalSteps}, degs:{currentAngle:2.2f} ")
            i = newestFile("/home/iemuser/mvme_krakow/mvme_data/listfiles")
            f, size = i[0], i[1].st_size
            if "/" in f: f = f.split("/")[-1]
            print(f"current file: {f}, size={size}")
            
            for s in range(t):
                sys.stdout.write(f"{t - s} secs remaining   \r")
                sys.stdout.flush()
                time.sleep(1)
                
            print(f"waited {t} seconds: endtime: {time.strftime('%H:%M:%S')}  ")
            print("enabling motor")
            self.write(COMMAND_ENABLE, 0, 0)
            time.sleep(2)
            self.motorEnabled=True
            self.emptyInputBuffer()
            # 2 move motor
            sys.stdout.write("moving motor....\r"); sys.stdout.flush()
            
            self.write(COMMAND_BACKWARD, steps, 0)
            totalSteps += steps
            currentAngle += steps * DEGS_PER_STEP

            time.sleep(steps * STEP_WAIT)
            self.emptyInputBuffer();
            print("motor moved %i steps %2.2f deg" % (steps, steps * DEGS_PER_STEP))
            sys.stdout.write("disabling motor..."); sys.stdout.flush()
            self.write(COMMAND_DISABLE, 0, 0)
            time.sleep(2)
            self.emptyInputBuffer()
            self.motorEnabled=False
 
        print("-------")
        print(f"Sweep finished. Final position after {totalSteps} steps: {currentAngle} degrees")
        print("================")
        
            
    def fn_swing(self):
        """
        makes a full sweep
        """
        initAngle = 0.0
        steps = 200         
        
        for i in range(20):
            
            for _ in range(1):
                self.write(COMMAND_FORWARD, steps, 0)
                time.sleep(steps * STEP_WAIT) # adapted to motor speed!!!
                time.sleep(1.5)
                self.emptyInputBuffer()
            
            for _ in range(1):
                self.write(COMMAND_BACKWARD, steps, 0)
                time.sleep(steps * STEP_WAIT)
                time.sleep(1.5)
                self.emptyInputBuffer()
        
        
    def fn_hardTest(self):
        """
        keeps moving forward the motor 200 steps, enabling and disabling
        300 iterations
        """
        for i in range(300):
            print("iteration %s" %i)
            self.write(COMMAND_ENABLE, 0, 0)
            time.sleep(1)
            self.write(COMMAND_BACKWARD, 200, 0)
            time.sleep(1)
            self.write(COMMAND_DISABLE, 0, 0)
            time.sleep(30)
        
    def fn_angle(self):
        """
        """
        print("angle:")
        self.write(COMMAND_ANGLE)
        
    def fn_commCheck(self):
        """
        checks communications with arduino
        """
        print("comms check")
        self.write(COMMAND_CHECK)
        
      
    def fn_homeSeek(self):
        """
        starts the home seeking routine in arduino
        """
        print("home seek")
        self.write(COMMAND_HOME)
    
    
    
    def fn_moveForward(self):
        """
        interactive function to move the motor forward
        """
        if not self.motorEnabled:
            print(colorize("MOTOR IS DISABLED. ENABLE IT BEFORE MOVING ", "lightred"))
            return

        while 1: 
            angle = input("angle in degrees? ")
            try:
                if int(angle) > 0:
                    break
                else:
                    print("angle must be positive. Use move backward instead")
                    return
            except:
                print("not a number")
        
        angle= int(angle)
        steps = int(angle / DEGS_PER_STEP)
        print(f"{angle} degrees -> {steps} steps")
        
        
        self.write(COMMAND_FORWARD, steps, 0)
        sys.stdout.write("moving motor....\r"); sys.stdout.flush()
        time.sleep(steps * STEP_WAIT)
        print(f"motor moved {angle} degrees forward")
        
    
    def fn_moveBackward(self):
        """
        interactive function to move the motor backward
        """
        if not self.motorEnabled:
            print(colorize("MOTOR IS DISABLED. ENABLE IT BEFORE MOVING ", "lightred"))
            return

        while 1: 
            angle = input("angle in degrees? ")
            try:
                if int(angle) > 0:
                    break
                else:
                    print("angle must be positive. Use move backward instead")
                    return
            except:
                print("not a number")
        
        steps = int(x / DEGS_PER_STEP)
        print(f"{angle} degrees -> {steps} step")
        
        
        self.write(COMMAND_BACKWARD, steps, 0)

        sys.stdout.write("moving motor....\r"); sys.stdout.flush()
        time.sleep(steps * STEP_WAIT)
        print(f"motor moved {angle} degrees backwards")
        
    def fn_rawCommand(self):
        """
        allows changing arduino pins
        """
        pin = int(input("pin number? "));
        value = int(input("value? "));
        
        if value != 0:
            value = 1
        
        self.write(COMMAND_RAW, pin, value);
        
        
    def fn_enable(self):
        """
        enables the motor
        """
        
        self.write(COMMAND_ENABLE, 0, 0);
        self.motorEnabled = True;
    
    
    def fn_disable(self):
        """
        disables the motor
        """
        self.write(COMMAND_DISABLE, 0, 0);
        self.motorEnabled = False;
    
    #---------------------------
    #  HELPER FUNCTIONS
    #---------------------------
    
    def emptyInputBuffer(self):
        """
        """
        if not DUMMY:
            while self.ser.inWaiting() > 0:  # Or: while ser.inWaiting():
                print("read:", str(self.readline()))
    
    def write(self, command, param1=0, param2=0):
        """
        writes a command to the arduino 
        @param command command issued to the motor (one of the constants above defined)
        @oaram param1 parameter 1
        @oaram param2 paramter 2
        """
        if DUMMY:
            print (colorize("[application in dummy mode: nothing is being sent to the motor]", "magenta"))
            return
        command = int(command)
        self.ser.write(bytes(f"{command} {param1} {param2}", "utf-8"))
        self.ser.flush()
        
    def readline(self):
        """
        reads a line from arduino serial interface
        """
        return self.ser.readline()[:-1].decode("utf-8")
    
    def reset_buffer(self):
        """
        """
        if not DUMMY:
            self.ser.reset_input_buffer()
        else:
            pass

    
if __name__ == "__main__":
    a = MotorControl(sys.argv[1]);
    a.interactive();
