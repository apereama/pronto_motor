#define FORWARD  true
#define BACKWARD false

// pins for the motor
#define PIN_DIRECTION 15   // ANALOG OUT1
#define PIN_DISABLE 16     // disables the motor. 16 = A2
#define PIN_STEP 7         // steps to the motor  D7
#define PIN_LED 13         //  

// pins for the encoder
#define PIN_ENC_A 3
#define PIN_ENC_B 4
#define PIN_ENC_C 2
 
 
int currentPos = 0;
volatile int angle;  // angle of the motor
volatile boolean encoderDir;

// motor keeps a counter
int motorCounter = 0;
int motorDirection = true;

// COMMANDS
#define COMMAND_FORWARD  0
#define COMMAND_BACKWARD  1
#define COMMAND_HOME  2
#define COMMAND_MOVE  3
#define COMMAND_POS  4
#define COMMAND_CHECK  5
#define COMMAND_RAW 6
#define COMMAND_ENABLE 7
#define COMMAND_DISABLE 8
#define COMMAND_ATHOME 9
#define COMMAND_ANGLE 9



/**
 *
 * Handler for interrupt 0: read the direction of turning
 * Is called on ENC_A rising and looks up ENC_B because:
 *
 *   - clockwise, when ENC_A goes high, ENCB is 0
 *   - anticlockwise, when ENC_A goes high, ENCB is 1  
 *
 *               ____
 *   ENC_A:  ___/    \________
 *                  ____
 *   ENC_B:  ______/    \________
 *                     ____
 *   ENC_C:  _________/    \________
 *
 */
void dirSet() 
{
  encoderDir = digitalRead(PIN_ENC_B);
  if (encoderDir ){ angle++; } else { angle--; }
  delayMicroseconds(1500);
}


/**
 * Handler for interrupt 1: updates the counter according with the direction
 */
void bar2()
{

//  if (encoderDir ){ angle++; } else { angle--; }
  if (digitalRead(PIN_LED)) {
    digitalWrite(PIN_LED, LOW);
  } else {
    digitalWrite(PIN_LED, HIGH);
  }

}



/**
 * Moves the motor the given number of steps
 *
 * @param dir direction
 * @param steps number of steps
 */
void moveMotor(boolean dir, int steps) {

  // 1) first, set the direction  
  if (dir == true) {
    digitalWrite(PIN_DIRECTION, HIGH);
    motorDirection = True
  } else {
     digitalWrite(PIN_DIRECTION, LOW);
    motorDirection = False
  }
    
  // move motor
  for (int i = 0; i < steps; i++) {
    
    digitalWrite(PIN_STEP, HIGH);
    digitalWrite(PIN_LED, HIGH);
    delay(20);
    digitalWrite(PIN_STEP, LOW);    
    digitalWrite(PIN_LED, LOW);
    delay(20);
    if (motorDirection) {motorCounter++;} else {motorCounter--;}
  }
  
}


/**
 * Initializes everything
 */
void setup() {

  Serial.begin(9600);  // initialize serial port
  
  // make the pins outputs:
  pinMode(19, INPUT);
  pinMode(PIN_DIRECTION, OUTPUT); // sets the direction for the motor
  pinMode(PIN_DISABLE, OUTPUT);   // pin to disable the motor
  pinMode(PIN_STEP, OUTPUT);      // pin to enable the motor
  pinMode(PIN_LED, OUTPUT);       // led
  
  // sets the pins from the encoder to digital read
  pinMode(PIN_ENC_A, INPUT);  // direction 1
  pinMode(PIN_ENC_B, INPUT);  // direction 2
  pinMode(PIN_ENC_C, INPUT);  // step

  digitalWrite(PIN_DISABLE,HIGH);
  angle = 1000;
  //attachInterrupt(0, bar, FALLING);  // D2, C goes to INT 0
  attachInterrupt(1, dirSet, FALLING); // D3, A goes to INT 1
  digitalWrite(PIN_DISABLE, LOW); 
  motorCounter = 0;
  
}

//------------------------ 
// main function
//
void loop() 
{
 
  if(false) {
    while(1) {
      Serial.print("ANGLE"); Serial.print(angle); Serial.print("DIR:"); Serial.print(encoderDir); Serial.print("\n");
      delay(100);
    }
  }
            
  while (Serial.available() > 0) { // if there's any serial available, read it:

    // look for t he next valid integer in the incoming serial stream:
    int command = Serial.parseInt(); 
    Serial.read();
    int param1 = Serial.parseInt(); 
    Serial.read();     
    int param2 = Serial.parseInt();     
    Serial.read();
    
    //Serial.print("COMMAND "); Serial.print(command); Serial.print("(");
    //Serial.print(param1); Serial.print(", ");
    //Serial.print(param2); Serial.print(")\n");
   
    while(Serial.peek() != -1) {      
      Serial.read();
    }
    
    
    //digitalWrite(PIN_LED,HIGH);
    //delay(400);
    //digitalWrite(PIN_LED,LOW);
    
    
    if (command == COMMAND_MOVE ) {
      
       if (param1 > 0) {
          moveMotor(FORWARD, param1);
          Serial.print("Moved FWD "); Serial.print(param1); Serial.print(" OK\n");   
       } else {
          moveMotor(BACKWARD, -param1);
          Serial.print("Moved BAK "); Serial.print(param1); Serial.print("OK\n");   
       }
       
    } else if (command == COMMAND_FORWARD) {
      moveMotor(FORWARD, param1);
      Serial.print("moved forward"); Serial.print(param1); 

    } else if (command == COMMAND_BACKWARD) {
      moveMotor(BACKWARD, param1);
      Serial.print("      moved backward"); Serial.print(param1); 
          
      
    } else if (command == COMMAND_CHECK) {
      Serial.print("COMMS OK \n");      
      
    } else if (command == COMMAND_POS) {
      Serial.print(motorDirection) + " " + Serial.print(MotorCounter); Serial.print("\n");
      Serial.print("OK\n");
      
    } else if (command == COMMAND_RAW) {
      pinMode(param1, OUTPUT);
      Serial.print("RAW()");
      
      if (param2 == 1) {
           digitalWrite(param1, HIGH);
           Serial.write("xxx1");
           
       } else if (param2 == 0) {
           digitalWrite(param1, LOW);
           Serial.write("xxx0");
           
       } else {
         digitalWrite(param1, LOW);
         Serial.print("ERROR on Param");
       }
       
       //Serial.print("LED"); Serial.print(param1); Serial.print(" "); Serial.print(param2); 
       
    } else if (command == COMMAND_ENABLE) {
      digitalWrite(PIN_DISABLE, HIGH);
      Serial.print("MOTOR ENABLED");
      
    } 
    
    else if (command == COMMAND_DISABLE)  {
      digitalWrite(PIN_DISABLE, LOW);
      Serial.print("MOTOR DISABLED");
    } 
    
    else if (command == COMMAND_ANGLE)  {
      Serial.print("A="); Serial.print(angle); Serial.print(" D="); Serial.print(encoderDir);
    } 
    
    else { 
      Serial.print("UNKNOWN "); Serial.print(command); 
    }
    Serial.print("( A="); Serial.print(angle); Serial.print(" D="); Serial.print(encoderDir); Serial.print(")");
    Serial.print("\n");
    
  } // while
  
} // loop
