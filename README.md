# pronto_code

This is the code repository for the motor control in the pronto experiment. It contains both the python3 code for control and the arduino embedded code.

## Contents

   - motor : code to move the motor and arduino embedded controller
   - xx 

## Installation

Assuming the hardware is connected and working:

create a screen, to be able to access from outside to the running script:

    screen -S motor_control

Remember, <code>screen -x motor_control</code> for attaching to the screen and <code>CTRL-A D</code> to detach from an screen.

inside the screen, go to the directory and run

   motor.sh

This shell script stores the program output in the file <code>positions.txt</code>

Once inside, check that everything is ok with the <code>comms check</code> options, and try moving the motor.

## Operation

##
